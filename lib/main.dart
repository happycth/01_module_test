import 'package:flutter/material.dart';
import 'movie/movie_list.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() {
    return _MyHomePageState();
  }
}

class _MyHomePageState extends State<MyHomePage> {
  List<String>title=[
    "用户反馈",
    "系统设置",
    "我要发布",
    "注销",
  ];

  List<IconData>iconlist=[
    Icons.feedback,
    Icons.settings,
    Icons.send,
    Icons.exit_to_app
  ];

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return DefaultTabController(
      length: 3,
      child:  Scaffold(
        appBar: AppBar(title: Text("电影列表"),
          centerTitle: true,
          actions: <Widget>[
            IconButton(icon: Icon(Icons.search), onPressed: (){
              print("点击搜索按钮");

            }),
          ],

        ),
        drawer:new Drawer(
          child: ListView.builder(
              padding: EdgeInsets.all(0),
              itemCount: 5,
              itemBuilder: (BuildContext context, int position){
                if(position==0){
                  return item1(position);
                }/*else if(position==4){
                return itemdivider();
              }*/else{
                  return item2(position);
                }
              })  ,

        ) ,

        bottomNavigationBar:Container(
          decoration: BoxDecoration(
            color: Colors.black
          ),
          height: 50,
          child: TabBar(
            labelStyle: TextStyle(
              height: 0,
              fontSize: 10
            ),
            tabs: <Widget>[
              Tab(icon:  Icon(Icons.movie_filter),text: "正在热映",),
              Tab(icon:  Icon(Icons.movie_creation),text: "即将上映",),
              Tab(icon:  Icon(Icons.local_movies),text: "Top250",),
            ],
          ),
        ),
        body: TabBarView(
          children: <Widget>[
            Movielist(mt: "in_theaters",),
            Movielist(mt:"coming_soon"),
            Movielist(mt:"top250"),
          ],
        ),
      ),
    );
  }

   Widget item1(int index){
    return GestureDetector(
      child: UserAccountsDrawerHeader(
      accountEmail: Text("1693891473@qq.com"),
        accountName: Text("张冰"),
        currentAccountPicture: CircleAvatar(
          backgroundImage: NetworkImage( "http://yanxuan.nosdn.127.net/65091eebc48899298171c2eb6696fe27.jpg",),
        ),
        //美化当前控件
        decoration: BoxDecoration(
          image: DecorationImage(
            fit: BoxFit.cover,
            image: NetworkImage(
              "https://www.itying.com/images/flutter/3.png",
            )

          )
        ),

      ),
    );

   }
   Widget item2(int index){
    return ListTile(
      title: Text(title[index-1]),
      trailing: Icon(iconlist[index-1]),
    );
   }
   Widget  itemdivider(){
    return   new Divider();
   }

}

