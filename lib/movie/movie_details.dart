import 'package:flutter/material.dart';


/***
 * 创建人:xuqing
 * 创建时间：2020年8月30日16:11:49
 * 类说明：电影详情页
 *
 *
 *
 */

class MovieDetails extends StatefulWidget {
  final  String id;
  final String  detail;

  MovieDetails({Key key,this.id, this.detail}) : super(key: key);

  @override
  _MovieDetailsState createState() {
    return _MovieDetailsState();
  }
}

class _MovieDetailsState extends State<MovieDetails> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return  Scaffold(
      appBar: AppBar(
        title: Text("电影详情页面"),
        centerTitle: true,
      ),
      body: Container(
        child: Center(
          child:Text("获取到的id和  详情数据 ${widget.id}+${widget.detail}")
        ),
      ),
    );
  }






}