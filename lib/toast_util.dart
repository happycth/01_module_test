import 'package:toast/toast.dart';


/**
 * 创建人：xuqing
 * 创建时间：2020年8月30日00:16:41
 * 类说明：toast工具类
 *
 *
 */

class ToastUtil{
      static  void  showInfo(Object context,String  str){
        Toast.show(str, context,  gravity: Toast.CENTER);
      }
}


